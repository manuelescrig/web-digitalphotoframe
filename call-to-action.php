    <!-- Featured Section -->
    <section class="hero section section-featured has-text-centered">
        <div class="hero-body">
            <div class="container">
                <h6 class="hero-eyebrow">TRY NOW</h6>
                <h1 class="hero-subtitle">The Most Popular Photo Frame App</h1>
                <h2 class="hero-description mt-4 mb-5">
                    Download Digital Photo Frame App now <br />and start displaying
                    memories with ease.
                </h2>
                <div class="columns is-vcentered mt-6">
                    <!-- App Store Reviews -->
                    <div class="column is-vcentered">
                        <img class="app-store-featured px-6" src="/images/svg/app-store-featured.svg"
                            alt="Digital Photo Frame App store Featured" />
                    </div>

                    <!-- App Store Feature -->
                    <div class="column is-vcentered">
                        <img class="app-store-featured px-6" src="/images/svg/app-store-reviews.svg"
                            alt="Digital Photo Frame App store Featured" />
                    </div>
                </div>
            </div>
        </div>
    </section>