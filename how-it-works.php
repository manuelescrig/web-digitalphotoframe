<!DOCTYPE html>
<html>

<head>
    <!-- Header-Top -->
    <?php include 'header-top.php';?>

    <!-- Social -->
    <!-- Primary Meta Tags -->
    <title>How It Works | Digital Photo Frame App - Photos & Videos Slideshow Player</title>
    <meta name="title" content="How It Works | Digital Photo Frame App - Photos & Videos Slideshow Player">
    <meta name="description" content="Discover all the features from Digital Photo Frame App. Download the App and start reliving the best memories of your life now.">

    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:url" content="https://digitalphotoframeapp.com/how-it-works/">
    <meta property="og:title" content="Digital Photo Frame App - Photos & Videos Slideshow Player">
    <meta property="og:description" content="Discover all the features from Digital Photo Frame App. Download the App and start reliving the best memories of your life now.">
    <meta property="og:image" content="https://digitalphotoframeapp.com/images/social/Digital_Photo_Frame_App.png">
    <meta property="fb:app_id" content="519330621467436" />

    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:url" content="https://digitalphotoframeapp.com/how-it-works/">
    <meta property="twitter:title" content="Digital Photo Frame App - Photos & Videos Slideshow Player">
    <meta property="twitter:description" content="Discover all the features from Digital Photo Frame App. Download the App and start reliving the best memories of your life now.">
    <meta property="twitter:image" content="https://digitalphotoframeapp.com/images/social/Digital_Photo_Frame_App.png">
    <meta name="twitter:site" content="@DigitalFrameApp">
    <meta name="twitter:image:alt" content="Digital Photo Frame App for iPad">

</head>

<!-- Body -->

<body>
    <!-- Header -->
    <header id="header-how-it-works">
        <section class="hero">
            <!-- Hero head: will stick at the top -->
            <!-- Navigation-Bar -->
            <?php include 'navigation-bar.php';?>

            <div class="container my-5"></div>
            <!-- Hero content: will be in the middle -->
            <div class="hero-body"></div>

            <!-- Hero footer: will stick at the bottom -->
            <div class="hero-foot"></div>
        </section>
    </header>

    <!-- How It Works -->
    <section class="section column m-2">
        <!-- Title -->
        <div class="container is-max-desktop has-text-centered my-6">
            <h6 class="header-eyebrow">EFFORTLESS TO USE</h6>
            <h3 class="header-title">How it works</h3>
            <h2 class="header-description">All your memories in one App.</h2>
        </div>
        <!-- /Title -->

        <nav class="level"></nav>

        <div class="container is-max-desktop px-6">
            <div class="is-divider"></div>
        </div>

        <div class="container">

            <div class="columns">

                <div class="column is-5 is-offset-1">

                    <h2 class="header-title">Simple Steps</h2>
                    <p class="header-description">
                        Learn how to start displaying your memories.
                    </p>

                    <div class="my-5">
                        <img class="icon filter-accent" src="/images/svg/apple-black.svg"
                            alt="Digital Photo Frame - Apple" />
                        <p>Download the App from the App Store</p>
                    </div>

                    <div class="my-5">
                        <img class="icon filter-accent" src="/images/svg/image.svg"
                            alt="Digital Photo Frame - Albums" />
                        <p>
                            Choose the album or multiple albums from the different sources you
                            want to start displaying
                        </p>
                    </div>

                    <div class="my-5">
                        <img class="icon filter-accent" src="/images/svg/play-circle.svg"
                            alt="Digital Photo Frame - Memories" />
                        <p>Press the play button and start reliving your moments</p>
                    </div>

                    <div class="my-5">
                        <img class="icon filter-accent" src="/images/svg/film.svg" alt="Digital Photo Frame - Enjoy" />
                        <p>That's all. Sit back & enjoy your memories!</p>
                    </div>

                    <a href="#features">
                        <button class="my-3 button is-rounded button-color">
                            See Features
                        </button>
                    </a>
                </div>

                <div class="column is-5 is-offset-1">
                    <picture>
                        <source srcset="/images/how-it-works/digital-photo-frame.webp" type="image/webp" />
                        <source srcset="/images/how-it-works/digital-photo-frame.png" type="image/png" />
                        <img src="/images/how-it-works/digital-photo-frame.png" alt="Digital Photo Frame App"
                            class="" />
                    </picture>
                </div>
            </div>
        </div>

    </section>
    <!-- /How It Works -->

    <nav class="level" id="features"></nav>

    <!-- Features -->
    <section class="section-features my-6">
        <div class="container has-text-centered">
            <div class="container is-max-desktop px-6">
                <div class="is-divider"></div>
            </div>

            <!-- Features -->
            <div class="columns">
                <div class="column is-three-fifths is-offset-one-fifth my-2">
                    <h6 class="header-eyebrow">TECHNOLOGY</h6>
                    <h2 class="header-title">Features</h2>
                    <p>
                        Discover all the features that Digital Photo Frame App offers.
                    </p>
                </div>
            </div>

            <!-- Features List -->
            <div class="columns">
                <div class="column my-4">
                    <div class="image is-24x24 container my-4">
                        <img class="filter-accent" src="/images/svg/video.svg" alt="Digital Photo Frame - Video" />
                    </div>
                    <h4 class="header-feature">
                        Video <span class="tag is-rounded">NEW</span>
                    </h4>
                    <p>With video, your slideshow becomes even more alive.</p>
                </div>
                <div class="column my-4">
                    <div class="image is-24x24 container my-4">
                        <img class="filter-accent" src="/images/svg/dot-circle.svg"
                            alt="Digital Photo Frame - Live Photos" />
                    </div>
                    <h4 class="header-feature">Live Photos</h4>
                    <p>The only Digital Photo Frame playing Apple Live Photos.</p>
                </div>
                <div class="column my-4">
                    <div class="image is-24x24 container my-4">
                        <img class="filter-accent" src="/images/svg/moon.svg" alt="Digital Photo Frame - Dark Mode" />
                    </div>
                    <h4 class="header-feature">Dark Mode</h4>
                    <p>Enjoy our beautifully designed Dark Mode throughout the App.</p>
                </div>
                <div class="column my-4">
                    <div class="image is-24x24 container my-4">
                        <img class="filter-accent" src="/images/svg/tablet.svg" alt="Digital Photo Frame - Tablet" />
                    </div>
                    <h4 class="header-feature">Any Device Size</h4>
                    <p>
                        We support any device from the smallest iPhone to the largest
                        iPad.
                    </p>
                </div>
            </div>
            <div class="columns">
                <div class="column my-4">
                    <div class="image is-24x24 container my-4">
                        <img class="filter-accent" src="/images/svg/window-restore.svg"
                            alt="Digital Photo Frame - Restoration State" />
                    </div>
                    <h4 class="header-feature">Restoration State</h4>
                    <p>
                        The app will continue playing from where it stops if you lock the
                        device for the night.
                    </p>
                </div>
                <div class="column my-4">
                    <div class="image is-24x24 container my-4">
                        <img class="filter-accent" src="/images/svg/film.svg" alt="Digital Photo Frame - Ken Burns" />
                    </div>
                    <h4 class="header-feature">Transition Types and Speed</h4>
                    <p>
                        Choose from more than 10 different transition types, including Ken
                        Burns effect.
                    </p>
                </div>
                <div class="column my-4">
                    <div class="image is-24x24 container my-4">
                        <img class="filter-accent" src="/images/svg/random.svg" alt="Digital Photo Frame - Shuffle" />
                    </div>
                    <h4 class="header-feature">Shuffle</h4>
                    <p>
                        Let the App algorithm create a unique slideshow experience every
                        time.
                    </p>
                </div>
                <div class="column my-4">
                    <div class="image is-24x24 container my-4">
                        <img class="filter-accent" src="/images/svg/sync.svg"
                            alt="Digital Photo Frame - Auto-updates" />
                    </div>
                    <h4 class="header-feature">Auto-Updates</h4>
                    <p>
                        New photos transition in constantly so you can see all the latest
                        photos and videos.
                    </p>
                </div>
            </div>
            <div class="columns">
                <div class="column my-4">
                    <div class="image is-24x24 container my-4">
                        <img class="filter-accent" src="/images/svg/search.svg" alt="Digital Photo Frame - Search" />
                    </div>
                    <h4 class="header-feature">Quick Search</h4>
                    <p>
                        Find your content faster by searching through your collections,
                        albums, moments...
                    </p>
                </div>
                <div class="column my-4">
                    <div class="image is-24x24 container my-4">
                        <img class="filter-accent" src="/images/svg/clock.svg" alt="Digital Photo Frame - Timer" />
                    </div>
                    <h4 class="header-feature">Timer (Night time)</h4>
                    <p>
                        Set it to start and stop based on a daily schedule. Perfect for
                        both the home and office.
                    </p>
                </div>
                <div class="column my-4">
                    <div class="image is-24x24 container my-4">
                        <img class="filter-accent" src="/images/svg/check-square.svg"
                            alt="Digital Photo Frame - Selection" />
                    </div>
                    <h4 class="header-feature">Selection</h4>
                    <p>
                        Play all the media or select which specific albums or items you
                        want to show.
                    </p>
                </div>
                <div class="column my-4">
                    <div class="image is-24x24 container my-4">
                        <img class="filter-accent" src="/images/svg/text-height.svg" alt="Digital Photo Frame - Text" />
                    </div>
                    <h4 class="header-feature">Font Size</h4>
                    <p>
                        The whole App is built with dynamic typography and adaptive
                        layouts.
                    </p>
                </div>
            </div>

            <div class="container is-max-desktop px-6">
                <div class="is-divider"></div>
            </div>

            <!-- On-Screen Information -->
            <div class="columns">
                <div class="column is-three-fifths is-offset-one-fifth my-2">
                    <h6 class="header-eyebrow">STAY UP-TO-DATE</h6>
                    <h2 class="header-title">On-Screen Information</h2>
                </div>
            </div>

            <!-- On-Screen Information List -->
            <div class="columns">
                <div class="column my-4">
                    <div class="image is-24x24 container my-4">
                        <img class="filter-accent" src="/images/svg/cloud-sun-rain.svg"
                            alt="Digital Photo Frame - Weather" />
                    </div>
                    <h4 class="header-feature">Weather</h4>
                    <p>
                        Display real-time weather information on top of the slideshow.
                    </p>
                </div>
                <div class="column my-4">
                    <div class="image is-24x24 container my-4">
                        <img class="filter-accent" src="/images/svg/calendar-alt.svg"
                            alt="Digital Photo Frame - Date and Time" />
                    </div>
                    <h4 class="header-feature">Date and Time</h4>
                    <p>
                        Display the current date and time on the slideshow. Use it as a
                        calendar.
                    </p>
                </div>
                <div class="column my-4">
                    <div class="image is-24x24 container my-4">
                        <img class="filter-accent" src="/images/svg/smog.svg"
                            alt="Digital Photo Frame - Air Quality Index" />
                    </div>
                    <h4 class="header-feature">Air Quality Index</h4>
                    <p>
                        Display the current Air Quality Index from your location.
                        Convenient for your life.
                    </p>
                </div>
                <div class="column my-4">
                    <div class="image is-24x24 container my-4">
                        <img class="filter-accent" src="/images/svg/umbrella-beach.svg"
                            alt="Digital Photo Frame - UV Index" />
                    </div>
                    <h4 class="header-feature">UV Index</h4>
                    <p>
                        Display the current UV Index from your location so you are aware
                        before leaving the house.
                    </p>
                </div>
            </div>

            <div class="container is-max-desktop px-6">
                <div class="is-divider"></div>
            </div>

            <!-- Sources -->
            <div class="columns">
                <div class="column is-three-fifths is-offset-one-fifth my-2">
                    <h6 class="header-eyebrow">ALL YOUR CONTENT</h6>
                    <h2 class="header-title">Image Sources</h2>
                    <p>
                        If you'd like to support a new source, please feel free to contact
                        us and request it. We will continue working to add new sources
                        requested by our users.
                    </p>
                </div>
            </div>

            <!-- Sources List -->
            <div class="columns">
                <div class="column my-4">
                    <div class="image is-24x24 container my-4">
                        <img class="filter-accent" src="/images/svg/apple-black.svg"
                            alt="Digital Photo Frame - Photos App" />
                    </div>
                    <h4 class="header-feature">Photos App</h4>
                    <p>
                        It supports, Albums, Shared Albums (iCloud), Smart Albums &
                        Moments from your photo library.
                    </p>
                </div>
                <div class="column my-4">
                    <div class="image is-24x24 container my-4">
                        <img class="filter-accent" src="/images/svg/google.svg"
                            alt="Digital Photo Frame - Google Photos" />
                    </div>
                    <h4 class="header-feature">Google Photos</h4>
                    <p>
                        Display and relive your memories taken directly from your Google
                        Photos.
                    </p>
                </div>
                <div class="column my-4">
                    <div class="image is-24x24 container my-4">
                        <img class="filter-accent" src="/images/svg/flickr.svg" alt="Digital Photo Frame - Flickr" />
                    </div>
                    <h4 class="header-feature">Flickr</h4>
                    <p>
                        Display Flickr public photos and curated collections, search
                        photos by user or by any topic.
                    </p>
                </div>
                <div class="column my-4">
                    <div class="image is-24x24 container my-4">
                        <img class="filter-accent" src="/images/svg/camera.svg" alt="Digital Photo Frame - Unsplash" />
                    </div>
                    <h4 class="header-feature">Unsplash</h4>
                    <p>
                        Display beautiful images and collections from Unsplash. Search
                        them by any topic.
                    </p>
                </div>
            </div>
        </div>
    </section>
    <!-- /Features -->

    <!-- Section Promotional -->
    <?php include 'call-to-action.php';?>
    <!-- /Section Promotional -->

    <!-- Footer-Top -->
    <?php include 'footer-top.php';?>

    <!-- Footer-Bottom -->
    <?php include 'footer-bottom.php';?>

</body>

</html>