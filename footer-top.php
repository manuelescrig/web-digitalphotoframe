<footer class="footer footer-top">
    <div class="container is-max-desktop">
        <div class="columns">
            <div class="column is-2">
                <h5 class="title is-5">Digital Photo Frame</h5>
                <ul class="is-lower-alpha has-text-weight-bold">
                    <li><a class="" href="/">Home</a></li>
                    <li><a class="" href="/how-it-works/">How It Works</a></li>
                    <li><a class="" href="/help/">Help</a></li>
                    <li><a class="" href="/about/">About</a></li>
                    <li><a class="" href="/blog/">Blog</a></li>
                    <li><a class="" href="/testimonials/">Testimonials</a></li>
                </ul>
            </div>
            <div class="column is-4">
                <h5 class="title is-5">Free Promo Codes</h5>
                <p class="text-silver">
                    Subscribe to the Newsletter and discover new updates. We will just
                    email you with the important stuff. Sometimes free promo codes.
                </p>
                <!-- Begin Mailchimp Signup Form -->
                <link href="//cdn-images.mailchimp.com/embedcode/horizontal-slim-10_7.css" rel="stylesheet"
                    type="text/css" />
                <style type="text/css">
                #mce-EMAIL {
                    clear: left;
                    font: 14px Helvetica, Arial, sans-serif;
                    border: none;
                    border-radius: 5px !important;
                    padding-left: 15px;
                    padding-right: 25px;
                    color: #333;
                    min-width: 100px;
                }

                /* Add your own Mailchimp form style overrides in your site stylesheet or in this style block.
        We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
                </style>
                <div id="mc_embed_signup text-left">
                    <form
                        action="https://media.us13.list-manage.com/subscribe/post?u=acda532bdfc6e75078e7ea1f7&amp;id=e7e3d4f8ba"
                        method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate"
                        target="_blank" novalidate>
                        <div id="mc_embed_signup_scroll">
                            <input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL"
                                placeholder="Your email address" required />
                            <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                            <div style="position: absolute; left: -5000px" aria-hidden="true">
                                <input type="text" name="b_acda532bdfc6e75078e7ea1f7_e7e3d4f8ba" tabindex="-1"
                                    value="" />
                            </div>
                            <div class="clear">
                                <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe"
                                    class="button button-subscribe" />
                            </div>
                        </div>
                    </form>
                </div>

                <!--End mc_embed_signup-->
            </div>
            <div class="column is-4">
                <h5 class="title is-5">Download the App Now</h5>
                <p>
                    Download the App now and start reliving the best moments of your
                    life.
                </p>
                <a target="_blank" class=""
                    href="https://itunes.apple.com/us/app/digital-photo-frame-pro-slideshow-creator/id1219786089?ls=1&mt=8"><img
                        class="app-store" src="/images/svg/app-store.svg" alt="Digital Photo Frame App store" /></a>
            </div>
            <div class="column">
                <h5 class="title is-5">Social Media</h5>
                <!-- Social Media -->
                <ul class="social">
                    <li>
                        <a class="" href="https://www.facebook.com/digitalphotoframeapp/" target="_blank"><img
                                class="social-icon" src="/images/svg/facebook-f.svg" alt="Facebook" />
                        </a>
                    </li>
                    <li>
                        <a class="" href="https://www.instagram.com/photoframeapp/" target="_blank"><img
                                class="social-icon" src="/images/svg/instagram.svg" alt="Instagram" />
                        </a>
                    </li>
                    <li>
                        <a class="" href="https://twitter.com/digitalframeapp" target="_blank"><img class="social-icon"
                                src="/images/svg/twitter.svg" alt="Twitter" />
                        </a>
                    </li>
                </ul>
                <!-- /End Social Media -->
            </div>
        </div>
</footer>