<footer class="footer footer-bottom">
    <div class="columns">
        <div class="column is-four-fifths is-vcentered">
            <!-- Company Details -->
            <div class="content has-text-left is-small company-copyright">
                <p>
                <br>¹ Live Photos and Videos are only available for your device & iCloud libraries. 
                <br>* iPad and iPhone are registered trademarks of Apple Inc.
                <br><br>

                    Copyright © <script>
                    document.write(new Date().getFullYear());
                    </script>.
                    <a target="_blank" class="link-white" href="/privacy/">Privacy Policy,</a>
                    <a target="_blank" class="link-white" href="/terms/">Terms and Conditions</a>.
                    <a target="_blank" class="link-white" href="/DigitalPhotoFrame-PressKit.zip">Press Kit</a>. All
                    Rights Reserved. Designed and Developed with
                    <img class="coffee" src="/images/svg/mug-hot.svg" height="16px" width="16px" alt="Coffee" />by
                    <a class="link-white" href="https://www.escrig.net/" target="_blank" data-placement="top"
                        data-toggle="tooltip">Manuel Escrig</a>.
                </p>
            </div>
            <!-- /End Company Details -->
        </div>
    </div>
</footer>


<!-- Default Statcounter code for Digital Photo Frame
http://www.digitalphotoframeapp.com -->
<script type="text/javascript">
var sc_project=10056559; 
var sc_invisible=1; 
var sc_security="fe7ac5e0"; 
</script>
<script type="text/javascript"
src="https://www.statcounter.com/counter/counter.js"
async></script>
<noscript><div class="statcounter"><a title="Web Analytics"
href="https://statcounter.com/" target="_blank"><img
class="statcounter"
src="https://c.statcounter.com/10056559/0/fe7ac5e0/1/"
alt="Web Analytics"></a></div></noscript>
<!-- End of Statcounter Code -->